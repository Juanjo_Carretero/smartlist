package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.Usuario;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserDetailsServiceImpl;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String correo) throws UsernameNotFoundException {
		Usuario user = userRepository.findById(correo)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with email: " + correo));

		return UserDetailsImpl.build(user);
	}

}
