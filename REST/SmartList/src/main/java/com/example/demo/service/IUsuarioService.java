package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Usuario;

public interface IUsuarioService {
	
public List<Usuario> listarUsuarios(); //Listar All 
	
	public Usuario guardarUsuario(Usuario usuario);	//Guarda un Curso CREATE
	
	public Usuario usuarioXID(String id); //Leer datos de un Curso READ
	
	public Usuario actualizarUsuario(Usuario usuario); //Actualiza datos del Curso UPDATE
	
	public void eliminarUsuario(String id);// Elimina el Curso DELETE
	
}
