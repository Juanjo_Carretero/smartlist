package com.example.demo.payload.response;

import java.util.Date;
import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String username;
	private String nom_apels;
	private String correo;
	private String password;
	private Date date_nacimiento;
	private List<String> roles;

	

	/**
	 * @param username
	 * @param nom_apels
	 * @param email
	 * @param password
	 * @param date_nacimiento
	 * @param roles
	 */
	public JwtResponse(String token ,String username, String nom_apels, String email, String password, Date date_nacimiento,
			List<String> roles) {
		this.token=token;
		this.username = username;
		this.nom_apels = nom_apels;
		this.correo = email;
		this.password = password;
		this.date_nacimiento = date_nacimiento;
		this.roles = roles;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the nom_apels
	 */
	public String getNom_apels() {
		return nom_apels;
	}

	/**
	 * @param nom_apels the nom_apels to set
	 */
	public void setNom_apels(String nom_apels) {
		this.nom_apels = nom_apels;
	}

	/**
	 * @return the email
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * @param email the email to set
	 */
	public void setCorreo(String email) {
		this.correo = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the date_nacimiento
	 */
	public Date getDate_nacimiento() {
		return date_nacimiento;
	}

	/**
	 * @param date_nacimiento the date_nacimiento to set
	 */
	public void setDate_nacimiento(Date date_nacimiento) {
		this.date_nacimiento = date_nacimiento;
	}

	/**
	 * @return the roles
	 */
	public List<String> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	
}
