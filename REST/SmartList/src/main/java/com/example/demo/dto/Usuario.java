package com.example.demo.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.demo.dto.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="usuario")
public class Usuario {
	
	@Id
	@Column(name="correo")
	private String id;
	
	@Column(name="nom_apels")
	private String nom_apels;
	
	@Column(name="password")
	private String password;
	
	@Column(name="username")
	private String username;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date_nacimiento;
	
	@OneToMany
	@JoinColumn(name="id")
	private List<UsuarioParticipaLista> usuarioParticipaLista;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "usuario_rol", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	

	/**
	 * 
	 */
	public Usuario() {
	}

	/**
	 * @param id
	 * @param nom_apels
	 * @param password
	 * @param username
	 * @param date_nacimiento
	 */
	public Usuario(String correo,String nom_apels, String password, String username, Date date_nacimiento) {
		this.id=correo;
		this.nom_apels = nom_apels;
		this.password = password;
		this.username = username;
		this.date_nacimiento = date_nacimiento;


	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the nom_apels
	 */
	public String getNom_apels() {
		return nom_apels;
	}

	/**
	 * @param nom_apels the nom_apels to set
	 */
	public void setNom_apels(String nom_apels) {
		this.nom_apels = nom_apels;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the date_nacimiento
	 */
	public Date getDate_nacimiento() {
		return date_nacimiento;
	}

	/**
	 * @param date_nacimiento the date_nacimiento to set
	 */
	public void setDate_nacimiento(Date date_nacimiento) {
		this.date_nacimiento = date_nacimiento;
	}
	
	

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	/**
	 * @return the usuarioParticipaLista
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "UsuarioParticipaLista")
	public List<UsuarioParticipaLista> getUsuarioParticipaLista() {
		return usuarioParticipaLista;
	}

	/**
	 * @param usuarioParticipaLista the usuarioParticipaLista to set
	 */
	public void setUsuarioParticipaLista(List<UsuarioParticipaLista> usuarioParticipaLista) {
		this.usuarioParticipaLista = usuarioParticipaLista;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nom_apels=" + nom_apels + ", password=" + password + ", username=" + username
				+ ", date_nacimiento=" + date_nacimiento + "]";
	}

	
	
	
}
