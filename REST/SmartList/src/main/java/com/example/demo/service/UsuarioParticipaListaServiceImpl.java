package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IUsuarioParticipaListaDao;
import com.example.demo.dto.UsuarioParticipaLista;

@Service
public class UsuarioParticipaListaServiceImpl  implements IUsuarioParticipaListaService{

	@Autowired
	IUsuarioParticipaListaDao usuarioParticipaListaDAO;
	
	@Override
	public List<UsuarioParticipaLista> listarUsuarioParticipaLista() {
		return usuarioParticipaListaDAO.findAll();
	}

	@Override
	public UsuarioParticipaLista guardarUsuarioParticipaLista(UsuarioParticipaLista usuarioParticipaLista) {
		return usuarioParticipaListaDAO.save(usuarioParticipaLista);
	}

	@Override
	public UsuarioParticipaLista usuarioParticipaListaXID(int id) {
		return usuarioParticipaListaDAO.findById(id).get();
	}

	@Override
	public UsuarioParticipaLista actualizarUsuarioParticipaLista(UsuarioParticipaLista usuarioParticipaLista) {
		return usuarioParticipaListaDAO.save(usuarioParticipaLista);
	}

	@Override
	public void eliminarUsuarioParticipaLista(int id) {
		usuarioParticipaListaDAO.deleteById(id);
		
	}

}
