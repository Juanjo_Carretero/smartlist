package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.Usuario;

@Repository
public interface UserRepository extends JpaRepository<Usuario, String> {
	Optional<Usuario> findById(String id);

	Boolean existsByUsername(String username);

	boolean existsById(String correo);
}
