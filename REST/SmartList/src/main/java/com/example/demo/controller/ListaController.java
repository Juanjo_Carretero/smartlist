package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;


import com.example.demo.dto.Lista;
import com.example.demo.service.ListaServiceImpl;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
public class ListaController {
	
	@Autowired
	ListaServiceImpl listaService;
	
	@GetMapping("/lista")
	public List<Lista> listarLista(){
		return listaService.listarLista();
	}
	
	@PostMapping("/lista")
	public Lista salvarLista(@RequestBody Lista lista) {
		
		return listaService.guardarLista(lista);
	}
	
	@GetMapping("/lista/{id}")
	public Lista ListaXcodigo(@PathVariable(name="id") int codigo) {
		
		Lista listaXcodigo= new Lista();
		
		listaXcodigo = listaService.ListaXcodigo(codigo);
		
		System.out.println("lista Xcodigo:  " + listaXcodigo);
		
		return listaXcodigo;
	}
	
	@PutMapping("/lista/{id}")
	public Lista actualizarProducto(@PathVariable(name="id")int codigo,@RequestBody Lista lista) {
		
		Lista lista_seleccionado= new Lista();
		Lista lista_actualizado= new Lista();
		
		lista_seleccionado= listaService.ListaXcodigo(codigo);
		
		lista_seleccionado.setCreador(lista.getCreador());
		lista_seleccionado.setFecha_creacion(lista.getFecha_creacion());
		lista_seleccionado.setNombre_lista(lista.getNombre_lista());
		lista_seleccionado.setDescripcion(lista.getDescripcion());
		
		lista_actualizado = listaService.actualizarLista(lista);
		
		System.out.println("La lista actualizada es: "+ lista_actualizado);
		
		return lista_actualizado;
	}
	
	@DeleteMapping("/lista/{id}")
	public void eliminarProducto(@PathVariable(name="id")int codigo) {
		listaService.eliminarLista(codigo);
	}


}
