package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.IProductoDao;
import com.example.demo.dto.Producto;


@Service
public class ProductoServiceImpl implements IProductoService{

	
	@Autowired
	IProductoDao iProductoDao;
	
	
	@Override
	public List<Producto> listarProductos() {
		return iProductoDao.findAll();
	}

	@Override
	public Producto guardarProducto(Producto producto) {
		return iProductoDao.save(producto);
	}

	@Override
	public Producto productoXCod_Producto(int cod_producto) {
		return iProductoDao.findById(cod_producto).get();
	}

	@Override
	public Producto actualizarProducto(Producto producto) {
		return iProductoDao.save(producto);
	}

	@Override
	public void eliminarProducto(int cod_producto) {
		iProductoDao.deleteById(cod_producto);
		
	}
}
