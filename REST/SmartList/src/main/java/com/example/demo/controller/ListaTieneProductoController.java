package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ListaTieneProducto;
import com.example.demo.service.IListaTieneProductoService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})

public class ListaTieneProductoController {
	
	@Autowired
	IListaTieneProductoService listaTieneProductoService;
	
	@GetMapping("/listaProducto")
	public List<ListaTieneProducto> listarListaTieneProducto(){
		return listaTieneProductoService.listarListaTieneProducto();
	}
	
	
	@PostMapping("/listaProducto")
	public ListaTieneProducto salvarListaTieneProducto(@RequestBody ListaTieneProducto listaTieneProducto) {
		
		return listaTieneProductoService.guardarListaTieneProducto(listaTieneProducto);
	}
	
	
	@GetMapping("/listaProducto/{id}")
	public ListaTieneProducto listaTieneProductoXID(@PathVariable(name="id") int id) {
		
		ListaTieneProducto listaTieneProducto_xid= new ListaTieneProducto();
		
		listaTieneProducto_xid=listaTieneProductoService.listaTieneProductoXID(id);
		
		System.out.println("Listas relacionadas con productos XID: "+listaTieneProducto_xid);
		
		return listaTieneProducto_xid;
	}
	
	@PutMapping("/listaProducto/{id}")
	public ListaTieneProducto actualizarListaTieneProducto(@PathVariable(name="id")int id,@RequestBody ListaTieneProducto listaTieneProducto) {
		
		ListaTieneProducto listaTieneProducto_seleccionado= new ListaTieneProducto();
		ListaTieneProducto listaTieneProducto_actualizado= new ListaTieneProducto();
		
		listaTieneProducto_seleccionado= listaTieneProductoService.listaTieneProductoXID(id);
		
		listaTieneProducto_seleccionado.setLista(listaTieneProducto.getLista());
		listaTieneProducto_seleccionado.setProducto(listaTieneProducto.getProducto());
		
		listaTieneProducto_actualizado = listaTieneProductoService.actualizarListaTieneProducto(listaTieneProducto_seleccionado);
		
		System.out.println("La relacion lista-producto actualizada es: "+ listaTieneProducto_actualizado);
		
		return listaTieneProducto_actualizado;
	}
	
	@DeleteMapping("/listaProducto/{id}")
	public void eleiminarListaTieneProducto(@PathVariable(name="id")int id) {
		listaTieneProductoService.eliminarListaTieneProducto(id);
	}

}
