package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="usuario_participa_lista")
public class UsuarioParticipaLista {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="correo_usuario")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name="codigo_lista")
	private Lista lista;
	
	@Column(name="rol")
	private String rol;
	
	@JsonProperty("lista")
	private void unpackNested(Integer codigo) {
	    this.lista = new Lista();
	    lista.setCodigo(codigo);
	}
	
	@JsonProperty("usuario")
	private void unpackNested(String correo) {
	    this.usuario = new Usuario();
	    this.usuario.setId(correo);
	}
	/**
	 * 
	 */
	public UsuarioParticipaLista() {
	}

	/**
	 * @param id
	 * @param usuario
	 * @param lista
	 * @param rol
	 */
	public UsuarioParticipaLista(int id, Usuario usuario, Lista lista, String rol) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.lista = lista;
		this.rol = rol;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the usuario
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the lista
	 */
	public Lista getLista() {
		return lista;
	}

	/**
	 * @param lista the lista to set
	 */
	public void setLista(Lista lista) {
		this.lista = lista;
	}

	/**
	 * @return the rol
	 */
	public String getRol() {
		return rol;
	}

	/**
	 * @param rol the rol to set
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}

	@Override
	public String toString() {
		return "UsuarioParticipaLista [id=" + id + ", usuario=" + usuario + ", lista=" + lista + ", rol=" + rol + "]";
	}
	
	
	
	
	
}
