package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.ListaTieneProducto;

public interface IListaTieneProductoDao extends JpaRepository<ListaTieneProducto, Integer>{

}
