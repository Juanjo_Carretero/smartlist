import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../service/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lateral',
  templateUrl: './lateral.component.html',
  styleUrls: ['./dashboard.css']
})
export class LateralComponent implements OnInit {

  constructor(
    private token: TokenStorageService,
    private router: Router
    ) { }

  ngOnInit(): void {

    var rutaActual = window.location.pathname;
    if(rutaActual == '/misSmartList'){
      $( '#misSmartList' ).addClass( 'active' );
    }else if(rutaActual == '/crearSmartList'){
      $( '#crearSmartList' ).addClass( 'active' );
    }else if(rutaActual == '/listarProducto'){
      $( '#listarProducto' ).addClass( 'active' );
    }else if(rutaActual == '/cuenta'){
      $( '#cuenta' ).addClass( 'active' );
    }else if(rutaActual == '/listarUsuario'){
      $( '#listarUsuario' ).addClass( 'active' );
    }else if(rutaActual == '/crearProducto'){
      $( '#crearProducto' ).addClass( 'active' );
    }

    console.log(this.token.getUser().roles)
    if(this.token.getUser().roles == "ROLE_ADMIN"){
      $('#crearProductos').css({'display':'block'});
      $('#listarUsuario').css({'display':'block'});
      $('#editar').css({'display':'block'});
      $('#eliminar').css({'display':'block'});
      $('#crearProducto').css({'display':'block'});



    }



  }

  public desconectar(){
    this.token.signOut();
    this.router.navigate(['inicio']);
  }
}
