import { Component } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { TokenStorageService } from './service/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SmartList';

  constructor(private router: Router, private token: TokenStorageService){}

  ngOnInit(): void {
    var rutaActual = window.location.pathname;
    if (sessionStorage.getItem('auth-token') == null) {
      if (rutaActual == '/') {
        this.router.navigate(['/inicio']);
      } else if (
        rutaActual != '/inicio' &&
        rutaActual != '/login' &&
        rutaActual != '/registro'
      ) {
        this.router.navigate(['/login']);
      }
    } else if ( rutaActual == '/inicio' || rutaActual == '/login' || rutaActual == '/registro')
      this.router.navigate(['/misSmartList']);
  }
}




