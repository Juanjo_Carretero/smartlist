export class Producto{
    cod_producto: number;
    nombre: string;
    precio: number;
    categoria: string;
    codigoQR: string;
    cantidad: number;
}
