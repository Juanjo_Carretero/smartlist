import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './Home/inicio/inicio.component';
import { PrincipalComponent } from './Navegacion/principal/principal.component';
import { LateralComponent } from './Navegacion/lateral/lateral.component';
import { CreateListaComponent } from './Cruds/Lista/create-lista/create-lista.component';
import { UpdateListaComponent } from './Cruds/Lista/update-lista/update-lista.component';
import { DeleteListaComponent } from './Cruds/Lista/delete-lista/delete-lista.component';
import { ListListaComponent } from './Cruds/Lista/list-lista/list-lista.component';
import { CrearUsuarioComponent } from './Cruds/Usuario/crear-usuario/crear-usuario.component';
import { BuscarUsuarioComponent } from './Cruds/Usuario/buscar-usuario/buscar-usuario.component';
import { ListProductoComponent } from './Cruds/Producto/list-producto/list-producto.component';
import { UpdateProductoComponent } from './Cruds/Producto/update-producto/update-producto.component';
import { CreateProductoComponent } from './Cruds/Producto/create-producto/create-producto.component';
import { DeleteProductoComponent } from './Cruds/Producto/delete-producto/delete-producto.component';
import { CuentaUsuarioComponent } from './Cruds/Usuario/cuenta-usuario/cuenta-usuario.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CreateUsuarioListaComponent } from './Cruds/UsuarioLista/create-usuario-lista/create-usuario-lista.component';
import { AbrirListaComponent } from './Cruds/Lista/abrir-lista/abrir-lista.component';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { E404Component } from './Info/e404/e404.component';
import { ListUsuarioComponent } from './Cruds/Usuario/list-usuario/list-usuario.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    PrincipalComponent,
    LateralComponent,
    CreateListaComponent,
    UpdateListaComponent,
    DeleteListaComponent,
    ListListaComponent,
    CrearUsuarioComponent,
    BuscarUsuarioComponent,
    ListProductoComponent,
    UpdateProductoComponent,
    CreateProductoComponent,
    DeleteProductoComponent,
    CuentaUsuarioComponent,
    CreateUsuarioListaComponent,
    AbrirListaComponent,
    E404Component,
    ListUsuarioComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
