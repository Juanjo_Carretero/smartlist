import { TestBed } from '@angular/core/testing';

import { UsuarioListaService } from './usuarioLista.service'

describe('UsuarioListaService', () => {
  let service: UsuarioListaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsuarioListaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
