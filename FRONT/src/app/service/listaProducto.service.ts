import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ListaProductoService {

  private baseUrl = '/api/listaProducto';

  constructor(private http: HttpClient) { }

  getProducto(cod_listaProducto: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${cod_listaProducto}`);
  }

  createProducto(listaProducto: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, listaProducto);
  }

  updateProducto(cod_listaProducto: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${cod_listaProducto}`, value);
  }

  deleteProducto(cod_listaProducto: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${cod_listaProducto}`, { responseType: 'text' });
  }

  getProductoList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
