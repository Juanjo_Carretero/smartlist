import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUsuarioListaComponent } from './create-usuario-lista.component';

describe('CreateUsuarioListaComponent', () => {
  let component: CreateUsuarioListaComponent;
  let fixture: ComponentFixture<CreateUsuarioListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateUsuarioListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUsuarioListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
