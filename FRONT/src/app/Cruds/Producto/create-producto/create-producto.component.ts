import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/model/producto';
import { ProductoService } from 'src/app/service/producto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-producto',
  templateUrl: './create-producto.component.html',
  styleUrls: ['./dashboard.css']
})
export class CreateProductoComponent implements OnInit {
  producto: Producto = new Producto();
  submitted = false;
  constructor(private productoService: ProductoService,
              private router: Router
    ) { }

  ngOnInit(): void {
  }
  newProducto(): void {
    this.submitted = false;
    this.producto = new Producto();
  }

  saveProducto() {

    this.productoService.createProducto(this.producto).subscribe(
      (data) => {
        this.producto = new Producto();
        alert("El producto se ha creado con éxito!")
        this.gotoList();
      },
      (error) => console.log(error)
    );
  }

  onSubmit() {
    this.submitted = true;
    this.saveProducto();
  }

  gotoList() {
    this.router.navigate(['/listarProducto']);
  }
}
