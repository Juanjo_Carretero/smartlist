import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model/usuario';
import { Observable } from 'rxjs';
import { UsuarioService } from '../../../service/usuario.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-usuario',
  templateUrl: './list-usuario.component.html',
  styleUrls: ['./dashboard.css']
})
export class ListUsuarioComponent implements OnInit {

  usuario: Observable<Usuario[]>;
  campoBusqueda: string;
  usuarioo: Usuario = new Usuario();
  constructor(private usuarioService: UsuarioService,
    private router: Router,
    ) {}

  ngOnInit() {

    this.reloadData();
  }

  reloadData() {
    this.usuario = this.usuarioService.getUsuarioList();
  }

  deleteProducto(correo: string) {
    this.usuarioService.deleteUsuario(correo)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  usuarioDetails(correo: string){
    this.router.navigate(['details', correo]);
  }

  updateUsuario(correo: string){
    this.router.navigate(['update', correo]);
  }

  buscar(){
    if($('#campoBusqueda').val().toString() != ""){
      this.campoBusqueda = $('#campoBusqueda').val().toString().toLowerCase().split(' ').join('')

    }else{
      this.campoBusqueda = null
    }

    this.usuarioo.nom_apels;

  }

}
