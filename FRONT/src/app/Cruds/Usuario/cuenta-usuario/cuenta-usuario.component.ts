import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from '../../../model/usuario';
import { UsuarioService } from '../../../service/usuario.service';
import { TokenStorageService } from '../../../service/token-storage.service';


@Component({
  selector: 'app-cuenta-usuario',
  templateUrl: './cuenta-usuario.component.html',
  styleUrls: ['./dashboard.css'],
})
export class CuentaUsuarioComponent implements OnInit {
  correo: string;
  usuario: Usuario = new Usuario();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usuarioService: UsuarioService,
    private tokenStorage: TokenStorageService
  ) {}

  ngOnInit() {

    this.correo = this.tokenStorage.getUser().correo;

    this.usuarioService.getUsuario(this.correo).subscribe(
      (data) => {
        console.log(data);
        this.usuario = data;
      },
      (error) => console.log(error)
    );
  }

  gotoList() {
    this.router.navigate(['/cuenta']);
  }

  onSubmit() {
    this.updateUsuario();
  }

  updateUsuario() {
    console.log(this.usuario);

    this.usuarioService.updateUsuario(this.correo, this.usuario).subscribe(
      (data) => {
        this.usuario = new Usuario();
      },
      (error) => console.log(error)
    );

    this.gotoList();
  }
}
