import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioListaService } from '../../../service/usuarioLista.service';
import { ListaService } from '../../../service/lista.service';
import { UsuarioLista } from '../../../model/usuarioLista';
import { TokenStorageService } from '../../../service/token-storage.service';

import { Lista } from '../../../model/lista';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-lista',
  templateUrl: './list-lista.component.html',
  styleUrls: ['./dashboard.css','list-lista.component.css'],
})
export class ListListaComponent implements OnInit {
  usuarioLista: UsuarioLista = new UsuarioLista();
  lista: Lista = new Lista();
  arrayLista: Lista[] = new Array();
  arrayUsuarioLista: UsuarioLista[] = new Array();
  arrayIDUsuarioLista: number[] = new Array();


  constructor(
    private listaService: ListaService,
    private usuarioListaService: UsuarioListaService,
    private router: Router,
    private token: TokenStorageService
  ) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.obtenerListasAsociadas();
  }

  obtenerListasAsociadas() {
    var p = this.usuarioListaService.getUsuarioListaList().subscribe(
      (data) => {
        data.forEach((element) => {
          if (element.usuario.id == this.token.getUser().correo) {
            this.arrayLista.push(element.lista);
          }
        });
      },
      (error) => console.log(error)
    );
  }

  listaDetail(codigo_lista: number){
    this.router.navigate(['/abrirSmartList', codigo_lista]);

  }

  deleteLista(cod_lista: number) {

    /**
     * CONSEGUIR RELACIONES CON LA LISTA INDICADA
     */
    var p = this.usuarioListaService.getUsuarioListaList()
    .subscribe(
      data => {
        data.forEach((element) => {
          if (element.lista.codigo == cod_lista ) {
            console.log(element)
            var c  = this.usuarioListaService.deleteUsuarioLista(element.id)
            .subscribe(
            data => {

              this.reloadPage();

            },

            error => console.log(error));
          }
        });
      },
      error => console.log(error));
      /**
       * Eliminar la Lista
       */



  }

  vaciaArrayLista(){
    this.arrayLista= new Array();
  }

  reloadPage() {
    window.location.reload();
}



  }
