import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ListaProducto } from 'src/app/model/listaProducto';
import { ListaProductoService } from '../../../service/listaProducto.service';
import { UsuarioListaService } from '../../../service/usuarioLista.service';
import { UsuarioService } from '../../../service/usuario.service';
import { Lista } from '../../../model/lista';
import { ListaService } from '../../../service/lista.service';
import { IfStmt, ThrowStmt } from '@angular/compiler';
import { Producto } from 'src/app/model/producto';
import { ProductoService } from 'src/app/service/producto.service';
import { Usuario } from 'src/app/model/usuario';
import { UsuarioLista } from 'src/app/model/usuarioLista';


@Component({
  selector: 'app-abrir-lista',
  templateUrl: './abrir-lista.component.html',
  styleUrls: ['./abrir-lista.component.css']
})
export class AbrirListaComponent implements OnInit {


  codigo_lista: number;
  lista: Lista = new Lista();
  listaProducto: ListaProducto;
  producto: Producto;
  arrayProducto: Producto[] = new Array();
  campoBusqueda: string;
  usuarioLista: UsuarioLista = new UsuarioLista();
  repetido: boolean = false;
  encontrado: boolean = false;
  idRelacion: number;

  constructor(private route: ActivatedRoute,private router: Router,
    private listaService: ListaService, private listaProductoService: ListaProductoService,
    private productoService: ProductoService, private usuarioListaService: UsuarioListaService,
    private usuarioService: UsuarioService) { }

  ngOnInit() {

    /*
        MOSTRAR DATOS DE LA SMARTLIST
    */

    this.codigo_lista = this.route.snapshot.params['codigo_lista'];
    this.mostrarDatosSmartList();



      this.obtenerProductosAsociados();
  }

  mostrarDatosSmartList(){
    this.listaService.getLista(this.codigo_lista)
      .subscribe(data => {
        //console.log(data)
        this.lista = data;

      }, error => console.log(error));
  }

  obtenerProductosAsociados(){
    this.arrayProducto = new Array();
    this.listaProductoService.getProductoList().subscribe(
      (data) => {
        data.forEach(element => {

          if (element.lista.codigo == this.codigo_lista) {
            this.arrayProducto.push(element.producto);

          }
        });
      }
    )
    console.log(this.arrayProducto);
  }



  newUsuarioLista(lista, usuario): void {
    this.usuarioLista = new UsuarioLista();
    this.usuarioLista.lista = lista;
    this.usuarioLista.usuario = usuario;
  }

  agregarUsuarioSmartList(){
    this.campoBusqueda = $('#correoIntroducido').val().toString();

    this.usuarioService.getUsuario(this.campoBusqueda).subscribe(data => {
      console.log(data)
      this.lista = data;

      this.newUsuarioLista(this.codigo_lista,this.campoBusqueda);

      this.usuarioListaService.getUsuarioListaList()
      .subscribe(
        data => {
          data.forEach((element) => {
            //Si no existe la relacion
            if (element.lista.codigo == this.codigo_lista
              && element.usuario.id == this.campoBusqueda) {
              console.log(element)
              this.repetido = true;
              alert("El usuario con correo "+this.campoBusqueda+" ya está en la lista!");
              this.mostrarDatosSmartList();
            }
          });

            if(!this.repetido){
              this.usuarioListaService
              .createUsuarioLista(this.usuarioLista)
              .subscribe(
                (data) => {
                  console.log(data);
                  alert("El usuario con correo "+this.campoBusqueda+" se ha agregado con éxito!")
                    this.mostrarDatosSmartList();
                },
                (error) => console.log(error)
                );
            }
      (error) => console.log(error)
      });

    }, error => alert("No se encuentra el correo introducido"));

    console.log(this.campoBusqueda)

  }

  eliminarUsuarioSmartList(){
    this.campoBusqueda = $('#correoIntroducido').val().toString();

    /**
     * CONSEGUIR RELACIONES CON LA LISTA INDICADA
     */
    var p = this.usuarioListaService.getUsuarioListaList()
    .subscribe(
      data => {
        data.forEach((element) => {

          if (element.usuario.id == this.campoBusqueda
            && element.lista.codigo == this.codigo_lista) {
              this.encontrado = true;

              this.idRelacion = element.id;
          }
        });

        if(this.encontrado){
          this.usuarioListaService.deleteUsuarioLista(this.idRelacion)
          .subscribe(
           data => {
             alert("El usuario con correo "+this.campoBusqueda+" se ha eliminado con éxito!")
             this.mostrarDatosSmartList();
             this.encontrado = false;

           },

           error => console.log(error));


        }else{
          alert("El usuario con correo "+this.campoBusqueda+" no se encuentra en esta lista!")
        }
      },
      error => console.log(error));

  }

  list(){
    this.router.navigate(['misSmartList']);
  }

  eliminarProductoSmartList(nombreProducto: string){
    console.log("hola")
    this.listaProductoService.getProductoList().subscribe(
      data => {
        data.forEach((element) => {
          console.log(element)
          if(element.producto.nombre == nombreProducto && element.lista.codigo == this.codigo_lista){
            this.listaProductoService.deleteProducto(element.id).subscribe(
              data => {
                alert("El producto "+nombreProducto+" se ha eliminado con éxito de la SmartList!")
                this.mostrarDatosSmartList();
                this.obtenerProductosAsociados();

              },

              error => console.log(error));
          }

          })
        });
  }
  reloadPage() {
    window.location.reload();
}

}
