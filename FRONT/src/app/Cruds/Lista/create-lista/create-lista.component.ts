import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/model/usuario';
import { UsuarioLista } from 'src/app/model/usuarioLista';
import { UsuarioService } from 'src/app/service/usuario.service';
import { UsuarioListaService } from 'src/app/service/usuarioLista.service';
import { Lista } from '../../../model/lista';
import { ListaService } from '../../../service/lista.service';
import { TokenStorageService } from '../../../service/token-storage.service';

@Component({
  selector: 'app-create-lista',
  templateUrl: './create-lista.component.html',
  styleUrls: ['./dashboard.css'],
})
export class CreateListaComponent implements OnInit {
  lista: Lista = new Lista();
  usuarioLista: UsuarioLista = new UsuarioLista();
  usuario: Usuario = new Usuario();
  submitted = false;

  constructor(
    private listaService: ListaService,
    private usuarioListaService: UsuarioListaService,
    private usuarioService: UsuarioService,
    private router: Router,
    private tokenStorage: TokenStorageService

  ) {}

  ngOnInit(
  ) {}

  newLista(): void {
    this.submitted = false;
    this.lista = new Lista();
  }

  newUsuarioLista(lista, usuario): void {
    this.submitted = false;
    this.usuarioLista = new UsuarioLista();
    this.usuarioLista.lista = lista;
    this.usuarioLista.usuario = usuario;
  }

  saveLista() {

    this.lista.creador = this.tokenStorage.getUser().username; //Cambiar por nombre del usuario logeado
    this.lista.fecha_creacion = this.tokenStorage.getUser().date_nacimiento;
    this.listaService.createLista(this.lista).subscribe(
      (data) => {
        this.lista = new Lista();

        /**
         * CREAR RELACION PERSONA LISTA
         */
        this.newUsuarioLista(data['codigo'],this.tokenStorage.getUser().correo);
        console.log(this.usuarioLista)
        this.usuarioListaService
          .createUsuarioLista(this.usuarioLista)
          .subscribe(
            (data) => {
              console.log(data);
              alert("La SmartList se ha creado con éxito!")
              this.gotoList();
            },
            (error) => console.log(error)
          );
      },
      (error) => console.log(error)
    );
  }


  onSubmit() {
    this.submitted = true;
    this.saveLista();
  }

  gotoList() {
    this.router.navigate(['/misSmartList']);
  }
}

function fecha() {
  var fechaActual = new Date().toLocaleDateString();
  var info = fechaActual.split('/');
  var año = info[2];
  var mes = info[1];
  var dia = info[0];
  var fecha_nacimiento = new Date(
    parseInt(año),
    parseInt(mes) - 1,
    parseInt(dia)
  );
  return fecha_nacimiento;
}
